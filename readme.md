## Train URI Manager Service for PHT: Quick Overview

**Train URI Manager Service** - This project aims to expose the services to simulate the *DOI.org* platform.
*Train URI Manager Service* is a platform which aims to be a lightweight and cheap to support the PHT project.
 It uses swagger UI API provides an easy way to test the rest service methods, as well as, provide a good overview of the model, including a descriptive documentation. 


<br/><br/>

## Development Environment Installation (Spring Boot Based)

First create the project folder.
> 1. ``` $ mkdir -p workspace/train_uri_managerservice ```

Clone the project.
> 2. ``` $ git clone https://git.rwth-aachen.de/PersonalHealthTrain/train_uri_managerservice.git ```

Open Eclipse (Or your any other IDE) and choose the path created above to start to edit the project. After that, you can use any IDE plugin or the command line bellow to start and run the application locally.

3. Then goto the project directory and run below command:
 ``` $ mvn spring-boot:run -Dspring-boot.run.arguments=--server.port=8888,--server.address=0.0.0.0,--spring.data.mongodb.host=127.0.0.1,--spring.data.mongodb.port=27017,--spring.data.mongodb.database=TrainURIManagerService,--mongo.host=127.0.0.1,--mongo.port=27017 ```




## Development Test Environment Installation (Docker Compose Based)

Download the uriService docker-compose
> 1. ``` $ wget https://git.rwth-aachen.de/PersonalHealthTrain/train_uri_managerservice/-/blob/master/docker-compose-uri.yml ```

Execute docker-compose
> 2. ``` $ docker-compose -f dockercompose.yml up -d ```


To test the APIs, goto the browser
> 3. ``` http://127.0.0.1:8888/TrainURIManagerService/swagger-ui.html#/sd-interfaces ```

<br/><br/>

## API documentation

Here, we have 5 types of APIs.
 1. ***Owner API*** - this is for signing or creating a new owner and generating a PID for the owner.
 2.  ***Prefix APIs*** - this is to access all prefix or by registered owner. 
 3. ***Suffix APIs*** - this is for adding , updating and  accessing suffix by different parameters.
 4.  ***DOI APIs*** - this is for accessing DOI repositories by different parameters.
 5. ***URI APIs*** - this is for accesign URI by different parameters.

### Owner API
API: ***train/service/uri/generate/pid/***
Descripction: Use this method to request a new PID/DOI generation, where the ownername (as the input parameters) represents the person/company/organization related to that new PID Object.

### Prefix APIs 
API: ***train/service/uri/findAll/prefixes/***
Descripction: Use this method to find all prefixes on the internal repository.

API: ***train/service/uri/findAll/prefix/byownername/byownername/***
Descripction: Use this method to find all prefixes on the internal repository which match the ownerName, as the input parameter

### Suffix APIs
API: ***train/service/uri/add/suffix/{prefix}/{version}/{state}***
Descripction: Use this method to request a new suffix for an existing doi (which should hold an preffix), where the prefix, version and state are the mandatory input parameters.

API: ***train/service/uri/findAll/suffiex/***
Descripction: Use this method to find all suffixes on the internal repository

API: ***train/service/uri/findAll/suffix/byownername/{ownerName}***
Descripction: Use this method to find all suffixes on the internal repository which match the ownerName, as the input parameter.

API: ***train/service/uri/findAll/suffix/byprefix/{prefix}***
Descripction: Use this method to find all suffixes on the internal repository which match the prefix, as the input parameter.

API: ***train/service/uri/findAll/suffix/byprefixstate/{prefix}{state}***
Descripction: Use this method to find all suffixes on the internal repository which match the prefix and state, as the input parameters.

API: ***train/service/uri/findAll/suffix/byprefixsuffixversion/{prefix}{version}***
Descripction: Use this method to find all suffixes on the internal repository which match the prefix and version, as the input parameters.

API: ***train/service/uri/findAll/suffix/byprefixversionstate/{prefix}/{version}/{state}***
Descripction: Use this method to find all suffixes on the internal repository which match the prefix, version and state, as the input parameters

API: ***train/service/uri/update/suffix/{prefix}/{suffix}/{version}/{state}***
Descripction: Use this method to edit the suffix parameters, such as version (the only two editable input parameters for this method). Plus the prefix and suffix are mandatory imputa parameters to edit the right object.

### DOI APIs
API: ***train/service/uri/findAll/repositorydoiobj/***
Descripction: Use this method to find all DOIs on the internal repository

API: ***train/service/uri/findAll/repositorydoiobj/byownername/{ownerName}/***
Descripction: Use this method to find all DOIs on the internal repository which match the ownername, as the input parameter

API: ***train/service/uri/findAll/repositorydoiobj/byprefix/***
Descripction: Use this method to find all DOIs on the internal repository which match the prefix, as the input parameter

### URI APIs
API: ***train/service/uri/findAll/byprefixsuffix/{prefix}/{suffix}***
Descripction: Use this method to find all URIs on the internal repository which match the prefix and suffix, as the input parameters.

API: ***train/service/uri/findAll/byprefixsuffixstate/{prefix}/{suffix}/{state}***
Descripction: Use this method to find all URIs on the internal repository which match the prefix, suffix and state as the input parameters.

API: ***train/service/uri/findAll/byprefixversionstate/{prefix}/{version}/{state}***
Descripction: Use this method to find all URIs on the internal repository which match the prefix, version and state as the input parameters.   
   



###**Authors**

* João Chaves [@jbjares](joao.bosco.jares.alves.chaves@fit.fraunhofer.de)
* Md. Rezaul Karim [@karim](md.rezaul.karim@fit.fraunhofer.de)
* Emon [@emon](abu.ibne.bayazid@fit.fraunhofer.de)


   -----
   <br/><br/>

#### Copyright and license

Copyright Fraunhofer FIT and RWTH Project under [the Apache 2.0 license](LICENSE).
   
   

   


 