package de.fraunhofer.fit.train.facade;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.log4j.LogXF;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import de.fraunhofer.fit.train.model.Prefix;
import de.fraunhofer.fit.train.model.Suffix;
import de.fraunhofer.fit.train.model.SufixStateEnum;
import de.fraunhofer.fit.train.model.Train4FAIRObject;
import de.fraunhofer.fit.train.persistence.IPrefix;
import de.fraunhofer.fit.train.persistence.ISuffix;
import de.fraunhofer.fit.train.persistence.ITrain4FAIRObjectRepository;

@EnableAspectJAutoProxy
@Service
public class ServiceFacade {
	

	
	@Autowired  
	private ITrain4FAIRObjectRepository t4fdoiRepository;

	
	@Autowired  
	private IPrefix prefixRepository;

	
	@Autowired  
	private ISuffix suffixRepository;
	
	@Autowired
	private MongoOperations mongoOps;
	
	
	@Autowired
	private Environment env;
	

	//====== GET Methods to query the t4f doi object ============
	
	public List<Train4FAIRObject>  findAll() {
		List<Train4FAIRObject> allList = new ArrayList<Train4FAIRObject>();
		Iterator<Train4FAIRObject> it = t4fdoiRepository.findAll().iterator();
		while(it.hasNext()) {
			allList.add(it.next());
		}
		return allList;
	}


	public List<Train4FAIRObject> findAllByOwnerName(String ownerName) {
		List<Train4FAIRObject> allList = new ArrayList<Train4FAIRObject>();
		Iterator<Train4FAIRObject> it = t4fdoiRepository.findAll().iterator();while(it.hasNext()) 
		{
			Train4FAIRObject t4fdoi = it.next();	
				if(t4fdoi.getPrefix()!=null && t4fdoi.getPrefix().getPrefixStr()!=null && !t4fdoi.getPrefix().getPrefixStr().equals("") && !t4fdoi.getPrefix().getOwnerName().equals("") && t4fdoi.getPrefix().getOwnerName().equals(ownerName)) {			
					allList.add(t4fdoi);
			}
		}
		return allList;
	}
	
	public List<Train4FAIRObject> findAllByPrefix(String preffix) {
		List<Train4FAIRObject> allList = new ArrayList<Train4FAIRObject>();
		Iterator<Train4FAIRObject> it = t4fdoiRepository.findAll().iterator();while(it.hasNext()) 
		{
			Train4FAIRObject t4fdoi = it.next();	
				if(t4fdoi.getPrefix()!=null && t4fdoi.getPrefix().getPrefixStr()!=null && !t4fdoi.getPrefix().getPrefixStr().equals("") && !t4fdoi.getPrefix().getOwnerName().equals("") && t4fdoi.getPrefix().getPrefixStr().equals(preffix)) {			
					allList.add(t4fdoi);
			}
		}
		return allList;
	}


	//====== GET Methods to query the prefixes doi object ============
	
	
	public List<Prefix>  findAllPreffixes() {
		List<Prefix> allList = new ArrayList<Prefix>();
		/*Iterator<Prefix> it = prefixRepository.findAll().iterator();
		while(it.hasNext()) {
			allList.add(it.next());
		} */		
		Iterator<Train4FAIRObject> it= t4fdoiRepository.findAll().iterator();
		while(it.hasNext()) {
			Train4FAIRObject t4obj= it.next();
			if(t4obj!=null && t4obj.getPrefix()!=null && 
			   !t4obj.getPrefix().getPrefixStr().equals("") && 
			   t4obj.getPrefix().getOwnerName()!=null &&
			   !t4obj.getPrefix().getOwnerName().equals("") &&
			   t4obj.getPrefix().getPrefixStr()!=null ) {
				allList.add(t4obj.getPrefix());
			}
		}		
		return allList;
	}


	public List<Prefix> findAllPrefixesByOwnerName(String ownerName) {
		List<Prefix> allList = new ArrayList<Prefix>();
		/*Iterator<Prefix> it = prefixRepository.findAll().iterator();
		while(it.hasNext()) 
		{
			Prefix prefix = it.next();	
				if(prefix!=null && prefix.getPrefixStr()!=null && !prefix.getPrefixStr().equals("") && !prefix.getOwnerName().equals("") && prefix.getOwnerName().equals(ownerName)) {			
					allList.add(prefix);
			}
		} */
		Iterator<Train4FAIRObject> it= t4fdoiRepository.findAll().iterator();
		while(it.hasNext()) {
			Train4FAIRObject t4obj= it.next();
			if(t4obj!=null && t4obj.getPrefix()!=null && 
			   !t4obj.getPrefix().getPrefixStr().equals("") && 
			   t4obj.getPrefix().getOwnerName()!=null &&
			   t4obj.getPrefix().getOwnerName().equals(ownerName) &&
			   t4obj.getPrefix().getPrefixStr()!=null) {
						allList.add(t4obj.getPrefix());
			   }
		}
		return allList;
	}


	//====== GET Methods to query the prefixes doi object ============
	
	
	public List<Suffix>  findAllSuffixes() {
		List<Suffix> suffixes = new ArrayList<Suffix>();
		Iterator<Train4FAIRObject> it = t4fdoiRepository.findAll().iterator();
		while(it.hasNext()) 
		{
			Train4FAIRObject t4fdoi = it.next();	
			if(t4fdoi.getPrefix()!=null &&
			   t4fdoi.getPrefix().getPrefixStr()!=null &&
               !t4fdoi.getPrefix().getPrefixStr().equals("") &&
               !t4fdoi.getPrefix().getOwnerName().equals("") && 
               t4fdoi.getSuffixes()!=null &&
               t4fdoi.getSuffixes().isEmpty()!=true ) {
					suffixes.addAll(t4fdoi.getSuffixes());
			  }
		}
		return suffixes;
	}


	public List<Suffix> findAllSuffixesByOwnerName(String ownerName) {
			List<Suffix> suffixes = new ArrayList<Suffix>();

			Prefix prefix = findPrefixByOwnername(ownerName);
			if(prefix!=null && !prefix.getPrefixStr().equals("")) {

				Train4FAIRObject t4fdoiBasedOnTheGivenOwnerNameAsInput = findt4fdoiByPrefix(prefix.getPrefixStr());
				if(t4fdoiBasedOnTheGivenOwnerNameAsInput!=null) {
					suffixes.addAll(t4fdoiBasedOnTheGivenOwnerNameAsInput.getSuffixes());
				}			
				
			}
		return suffixes;
	}

	

	public List<Suffix> findAllSufixesByPrefix(String prefixStr) {
		List<Suffix> suffixes = new ArrayList<Suffix>();

		if(prefixStr!=null && !prefixStr.equals("")) {

			Train4FAIRObject t4fdoiBasedOnTheGivenPrefixAsInput = findt4fdoiByPrefix(prefixStr);
			if(t4fdoiBasedOnTheGivenPrefixAsInput!=null) {
				suffixes.addAll(t4fdoiBasedOnTheGivenPrefixAsInput.getSuffixes());
			}
			
			
		}

			
	

	return suffixes;
}





	private Train4FAIRObject findt4fdoiByPrefix(String prefixStr) {
			Iterator<Train4FAIRObject> it = t4fdoiRepository.findAll().iterator();
			while(it.hasNext()) 
			{
				Train4FAIRObject t4fdoi = it.next();	
				if(t4fdoi.getPrefix()!=null && 
				   t4fdoi.getPrefix().getPrefixStr()!=null &&
				   !t4fdoi.getPrefix().getOwnerName().equals("")  && 
				   t4fdoi.getPrefix().getPrefixStr().equals(prefixStr) ) {			
						return t4fdoi;
				}
			}
		
		return null;
	}


	private Prefix findPrefixByOwnername(String ownerName) {
		Iterator<Train4FAIRObject> it = t4fdoiRepository.findAll().iterator();while(it.hasNext()) 
		{
			Train4FAIRObject t4fdoi = it.next();	
				if(t4fdoi.getPrefix()!=null && t4fdoi.getPrefix().getPrefixStr()!=null && t4fdoi.getPrefix().getOwnerName().equals(ownerName)  && !t4fdoi.getPrefix().getPrefixStr().equals("")) {			

					if(t4fdoi.getPrefix()!=null) {
						return t4fdoi.getPrefix();
					}
					
			}
		}
	

	return null;
	}



	//====== POST Methods to create a new PID or t4f doi object ============
	
	public Train4FAIRObject generatePID(String ownername) {
		
		for(Train4FAIRObject t4f:findAllByOwnerName(ownername)) {
			if(t4f.getPrefix()==null) {
				continue;
			}
			
			if(t4f.getPrefix().getOwnerName()!=null && !t4f.getPrefix().getOwnerName().equals("") && t4f.getPrefix().getOwnerName().equals(ownername)) {
				return t4f;
			}
		}
		
		if(ownername!=null && !ownername.equals("") && ownername.equals(ownername)) {
			Train4FAIRObject pid = new Train4FAIRObject();
			Prefix prefix = new Prefix();
			prefix.setOwnerName(ownername);
			pid.setPrefix(prefix);
			t4fdoiRepository.save(pid);
			
			String pidProtocol = env.getProperty("pid.protocol");
			String pidHost = env.getProperty("pid.host");
			String pidPort = env.getProperty("pid.port");
			StringBuilder sb = new StringBuilder();
			sb.append(pidProtocol);
			sb.append(pidHost);
			sb.append(":");
			sb.append(pidPort);
			sb.append("/");
			if(pid.get_id()!=null) {
				sb.append(pid.get_id().toString());
				sb.append("/");
				String pidURI = sb.toString();
				pid.setUri(pidURI);	
				prefix.setPrefixStr(pid.get_id().toString());
				prefix.setPrefixURI(sb.toString());
				pid.setPrefix(prefix);

				return t4fdoiRepository.save(pid);
			}

		}

		
		return null;
	}

	
	
	public Train4FAIRObject attachSuffixVersionAndStateToPID(String prefix, String version, SufixStateEnum state) {
		
		if(prefix!=null) {
			
			Train4FAIRObject oldPid = findt4fdoiByPrefix(prefix);
				if(oldPid!=null && oldPid.get_id()!=null && !oldPid.get_id().toString().equals("")) {

						Suffix suffix = new Suffix();
						suffix = suffixRepository.save(suffix);
						if(suffix.get_id()==null || suffix.get_id().toString().equals("")) {
							return null;
						}
						String suffixUri = suffix.get_id().toString();
						String uri = oldPid.getUri();
						uri = uri + suffixUri +"/";
						suffix.setSuffixStr(suffixUri);
						suffix.setSuffixURI(uri);

						
						suffix.setVersion(version);
						suffix.setSufixStateEnum(state);
						List<Suffix> suffixes = oldPid.getSuffixes();
						
						if(suffixes==null || suffixes.isEmpty()) {
							suffixes = new ArrayList<Suffix>();
						}
						suffixes.add(suffix);
						oldPid.setSuffixes(suffixes);

						return t4fdoiRepository.save(oldPid);
					

				}

				

		}

		return null;
	}


	
	public List<String> findAllSufixesByPrefixVersion(String prefix, String version) {
		
		List<Train4FAIRObject> t4fobjList = findAll();
		List<String> result = new ArrayList<String>();
		
		for(Train4FAIRObject t4fobj:t4fobjList) {
			if(t4fobj==null || t4fobj.getPrefix()==null || !t4fobj.getPrefix().getPrefixStr().equals(prefix)) {
				continue;
			}
			
			List<Suffix> suffiex = t4fobj.getSuffixes();
			
			for(Suffix suffix: suffiex) {
				if(suffix==null || suffix.getVersion()==null) {
					continue;
				}
				if(suffix.getVersion().equals(version)) {
					result.add(suffix.getSuffixStr());
				}
				
			}

		}

		return result;
	}


	public List<String> findAllSufixesByPrefixState(String prefix, SufixStateEnum state) {
		List<Train4FAIRObject> t4fobjList = findAll();
		List<String> result = new ArrayList<String>();

		for(Train4FAIRObject t4fobj:t4fobjList) {
			if(t4fobj==null || t4fobj.getPrefix()==null || !t4fobj.getPrefix().getPrefixStr().equals(prefix)) {
				continue;
			}
	
			List<Suffix> suffixesList= t4fobj.getSuffixes();
	
			for(Suffix suffixLoop: suffixesList) {
				if(suffixLoop==null || suffixLoop.getSufixStateEnum()==null || suffixLoop.getSuffixStr()==null) {
						continue;
					}
				if(suffixLoop.getSufixStateEnum().getText().equals(state.getText())) {
						result.add(suffixLoop.getSuffixStr());
					}
	
			 }

		}

		return result;
	}


	public List<String> findAllSufixesByPrefixVersionState(String prefix, String version, SufixStateEnum state) {
		
		List<Train4FAIRObject> t4fobjList = findAll();
		List<String> result = new ArrayList<String>();
		
		for(Train4FAIRObject t4fobj:t4fobjList) {
			if(t4fobj==null || t4fobj.getPrefix()==null || !t4fobj.getPrefix().getPrefixStr().equals(prefix)) {
				continue;
			}
			
			List<Suffix> suffiex = t4fobj.getSuffixes();
			
			for(Suffix suffix: suffiex) {
				if(suffix==null || suffix.getSufixStateEnum()==null || suffix.getVersion()==null) {
					continue;
				}
				if(suffix.getSufixStateEnum().getText().equals(state.getText()) && suffix.getVersion().equals(version) ) {
					result.add(suffix.getSuffixStr());
				}

			}

		}

		return result;
	}


	public List<String> findAllURIByPrefixSuffix(String prefix, String suffix) {
		
		List<Train4FAIRObject> t4fobjList = findAll();
		List<String> result = new ArrayList<String>();
		
		for(Train4FAIRObject t4fobj:t4fobjList) {
			if(t4fobj==null || t4fobj.getPrefix()==null || !t4fobj.getPrefix().getPrefixStr().equals(prefix)) {
				continue;
			}
			
			List<Suffix> suffixesList= t4fobj.getSuffixes();
			
			for(Suffix suffixLoop: suffixesList) {
				if(suffixLoop==null ||suffixLoop.getSuffixStr()==null) {
					continue;
				}
				if(suffixLoop.getSuffixStr().toString().equals(suffix)) {
					result.add(suffixLoop.getSuffixURI());
				}

			}

		}

		return result;
	}


	public List<String> findAllURIByPrefixSuffixState(String prefix, String suffixinput, SufixStateEnum state) {
		List<Train4FAIRObject> t4fobjList = findAll();
		List<String> result = new ArrayList<String>();
		
		for(Train4FAIRObject t4fobj:t4fobjList) {
			if(t4fobj==null || t4fobj.getPrefix()==null || !t4fobj.getPrefix().getPrefixStr().equals(prefix)) {
				continue;
			}
			
			List<Suffix> suffixesList= t4fobj.getSuffixes();
			
			for(Suffix suffixLoop: suffixesList) {
				if(suffixLoop==null || suffixLoop.getSufixStateEnum()==null || suffixLoop.getSuffixStr()==null) {
					continue;
				}
				if(suffixLoop.getSuffixStr().toString().equals(suffixinput) && suffixLoop.getSufixStateEnum().getText().equals(state.getText())) {
					result.add(suffixLoop.getSuffixURI());
				}

			}

		}

		return result;

	}


	public List<String> findAllURIByPrefixVersionState(String prefix, String version, SufixStateEnum state) {
		List<Train4FAIRObject> t4fobjList = findAll();
		List<String> result = new ArrayList<String>();
		
		for(Train4FAIRObject t4fobj:t4fobjList) {
			if(t4fobj==null || t4fobj.getPrefix()==null || !t4fobj.getPrefix().getPrefixStr().equals(prefix)) {
				continue;
			}
			
			List<Suffix> suffixesList= t4fobj.getSuffixes();
			
			for(Suffix suffixLoop: suffixesList) {
				if(suffixLoop==null || suffixLoop.getSufixStateEnum()==null || suffixLoop.getSuffixStr()==null) {
					continue;
				}
				if(suffixLoop.getVersion().equals(version) && suffixLoop.getSufixStateEnum().getText().equals(state.getText())) {
					result.add(suffixLoop.getSuffixURI());
				}

			}

		}

		return result;
	}


	public Train4FAIRObject attachSuffixToPID(String prefix, String version, SufixStateEnum state) {
		List<Train4FAIRObject> t4fobjList = findAll();
		Train4FAIRObject result = new Train4FAIRObject();
		for(Train4FAIRObject t4fobj:t4fobjList) {
			if(t4fobj==null || t4fobj.getPrefix()==null || !t4fobj.getPrefix().getPrefixStr().equals(prefix)) {
				continue;
			}
			
			List<Suffix> suffixesList= t4fobj.getSuffixes();
			if(suffixesList==null) {
				return attachSuffixVersionAndStateToPID(prefix,version,state); 
			}
			
			for(Suffix suffixLoop: suffixesList) {
				if(suffixLoop==null || suffixLoop.getSufixStateEnum()==null || suffixLoop.getSuffixStr()==null) {
					continue;
				}

				suffixLoop.setVersion(version);
				suffixLoop.setSufixStateEnum(state);
				return attachSuffixVersionAndStateToPID(prefix,version,state);
				

			}

		}
		return result;
	}



	public Train4FAIRObject editAttachedSuffixToPID(String prefix, String suffix, String version,SufixStateEnum state) {	
		List<Train4FAIRObject> t4fobjList = findAll();
		for(Train4FAIRObject t4fobj:t4fobjList) {
			if(t4fobj==null || t4fobj.getPrefix()==null || !t4fobj.getPrefix().getPrefixStr().equals(prefix)) {
				continue;
			}
			
			List<Suffix> suffixesList= t4fobj.getSuffixes();
			
			for(Suffix suffixLoop: suffixesList) {
				if(suffixLoop==null || suffixLoop.getSufixStateEnum()==null || suffixLoop.getSuffixStr()==null) {
					continue;
				}
				if(suffixLoop.getSuffixStr().equals(suffix)) {
					
					suffixLoop.setVersion(version);
					suffixLoop.setSufixStateEnum(SufixStateEnum.fromString(state.getText()));
					return t4fdoiRepository.save(t4fobj);
				}

			}

		}
		return null;
		
	}




	
	

}
