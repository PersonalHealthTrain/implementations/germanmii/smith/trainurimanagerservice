package de.fraunhofer.fit.train.persistence;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import de.fraunhofer.fit.train.model.Prefix;
import de.fraunhofer.fit.train.model.Train4FAIRObject;

public interface IPrefix extends CrudRepository<Prefix, String>, QueryByExampleExecutor<Prefix> {

	@Query("{ '?0' : { $regex: ?1 } }")
	List<Prefix> findOneByQuery(String fieldStr,String cotent);

	@Query("{ ?0 : { $regex: ?1 } }")
	List<Prefix> findOneBySmampleQuery(String fieldStr,String cotent);
	
	@Query(value="{}", fields="{ '?0' : ?1}")
	List<Prefix> findOneBySimpleQuery(String fieldStr,String cotent);

	@Query("{ ?0 : { $regex: ?1 } }")
	List<Prefix> findOneByRegexQuery(String fieldStr, String cotent);
	
	
}
