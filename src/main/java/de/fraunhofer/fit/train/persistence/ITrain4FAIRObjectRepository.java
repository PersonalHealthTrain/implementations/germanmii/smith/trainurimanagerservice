package de.fraunhofer.fit.train.persistence;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import de.fraunhofer.fit.train.model.Train4FAIRObject;

public interface ITrain4FAIRObjectRepository extends CrudRepository<Train4FAIRObject, String>, QueryByExampleExecutor<Train4FAIRObject> {

	@Query("{ '?0' : { $regex: ?1 } }")
	List<Train4FAIRObject> findOneByQuery(String fieldStr,String cotent);

	@Query("{ ?0 : { $regex: ?1 } }")
	List<Train4FAIRObject> findOneBySmampleQuery(String fieldStr,String cotent);
	
	@Query(value="{}", fields="{ '?0' : ?1}")
	List<Train4FAIRObject> findOneBySimpleQuery(String fieldStr,String cotent);

	@Query("{ ?0 : { $regex: ?1 } }")
	List<Train4FAIRObject> findOneByRegexQuery(String fieldStr, String cotent);
	
	
}
