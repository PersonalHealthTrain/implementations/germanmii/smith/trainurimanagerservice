package de.fraunhofer.fit.train.persistence;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import de.fraunhofer.fit.train.model.Suffix;

public interface ISuffix extends CrudRepository<Suffix, String>, QueryByExampleExecutor<Suffix> {

	@Query("{ '?0' : { $regex: ?1 } }")
	List<Suffix> findOneByQuery(String fieldStr,String cotent);

	@Query("{ ?0 : { $regex: ?1 } }")
	List<Suffix> findOneBySmampleQuery(String fieldStr,String cotent);
	
	@Query(value="{}", fields="{ '?0' : ?1}")
	List<Suffix> findOneBySimpleQuery(String fieldStr,String cotent);

	@Query("{ ?0 : { $regex: ?1 } }")
	List<Suffix> findOneByRegexQuery(String fieldStr, String cotent);
	
	
}
