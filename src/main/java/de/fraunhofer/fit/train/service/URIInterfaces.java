package de.fraunhofer.fit.train.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fraunhofer.fit.train.facade.ServiceFacade;
import de.fraunhofer.fit.train.model.Prefix;
import de.fraunhofer.fit.train.model.Suffix;
import de.fraunhofer.fit.train.model.SufixStateEnum;
import de.fraunhofer.fit.train.model.Train4FAIRObject;
import de.fraunhofer.fit.train.util.TrainUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@Api(description = "Endpoints to support the URI Management which aims to be a DOI repository for the PHT project. Similar Datacite.",tags = {"uri-manager-service"})
@EnableAspectJAutoProxy
@RestController
@Service
public class URIInterfaces {

	@Autowired
	private ServiceFacade facade;
	
	
	
	//====== GET Methods to query the t4f doi object ============
	
	@ApiOperation("Use this method to find all DOIs on the internal repository")
	@GetMapping(value = "/train/service/uri/findAll/repositorydoiobj/",produces = MediaType.APPLICATION_JSON_VALUE)
	List<Train4FAIRObject> findAllObjectOnTheRepository() throws Exception {
		List<Train4FAIRObject> allList = new ArrayList<Train4FAIRObject>();
		allList = facade.findAll();
		
		if(!allList.isEmpty() || allList !=null) {
			return allList;
		}
		return Collections.EMPTY_LIST;
	}
	
	@ApiOperation("Use this method to find all DOIs on the internal repository which match the ownername, as the input parameter")
	@GetMapping(value = "/train/service/uri/findAll/repositorydoiobj/byownername/{ownerName}/", produces = MediaType.APPLICATION_JSON_VALUE)
	List<Train4FAIRObject> findAllObjectOnTheRepositoryByOwnerName(
			@ApiParam("The unique identifier, called here as ownername. The name of the owner of the PID")@PathVariable String ownerName) throws Exception {
		List<Train4FAIRObject> allList = new ArrayList<Train4FAIRObject>();
		allList = facade.findAllByOwnerName(ownerName);
		
		if(!allList.isEmpty() || allList !=null) {
			return allList;
		}
		return Collections.EMPTY_LIST;
	}
	
	
	@ApiOperation("Use this method to find all DOIs on the internal repository which match the prefix, as the input parameter")
	@GetMapping(value = "/train/service/uri/findAll/repositorydoiobj/byprefix/{prefix}/", produces = MediaType.APPLICATION_JSON_VALUE)
	List<Train4FAIRObject> findAllObjectOnTheRepositoryByThePrefix(@ApiParam("The number received after the call of method generate PID. This number will be forever associated with one and only one ownername, and can has multiple suffix.")@PathVariable String prefix) throws Exception {
		List<Train4FAIRObject> allList = new ArrayList<Train4FAIRObject>();
		allList = facade.findAllByPrefix(prefix);
		
		if(!allList.isEmpty() || allList !=null) {
			return allList;
		}
		return Collections.EMPTY_LIST;
	}
	
	
	//====== GET Methods to query the prefixes doi object ============
	
	@ApiOperation("Use this method to find all prefixes on the internal repository")
	@GetMapping(value = "/train/service/uri/findAll/prefixes/", produces = MediaType.APPLICATION_JSON_VALUE)
	List<Prefix> findAllPrefixes() throws Exception {
		List<Prefix> allList = new ArrayList<Prefix>();
		allList = facade.findAllPreffixes();
		
		if(!allList.isEmpty() || allList !=null) {
			return allList;
		}
		return Collections.EMPTY_LIST;
	}
	
	
	@ApiOperation("Use this method to find all prefixes on the internal repository which match the ownerName, as the input parameter")
	@GetMapping(value = "/train/service/uri/findAll/prefix/byownername/{ownerName}/",  produces = MediaType.APPLICATION_JSON_VALUE)
	List<Prefix> findAllPrefixesByOwnerName(@ApiParam("The unique identifier, called here as ownername. The name of the owner of the PID") @PathVariable String ownerName) throws Exception {
		List<Prefix> allList = new ArrayList<Prefix>();
		allList = facade.findAllPrefixesByOwnerName(ownerName);
		
		if(!allList.isEmpty() || allList !=null) {
			return allList;
		}
		return Collections.EMPTY_LIST;
	}
	
	
	//====== GET Methods to query the sufix doi object ============
	
	
	@ApiOperation("Use this method to find all suffixes on the internal repository")
	@GetMapping(value = "/train/service/uri/findAll/suffiex/",  produces = MediaType.APPLICATION_JSON_VALUE)
	List<Suffix> findAllSuffixes() throws Exception {
		List<Suffix> allList = new ArrayList<Suffix>();
		allList = facade.findAllSuffixes();
		
		if(!allList.isEmpty() || allList !=null) {
			return allList;
		}
		return Collections.EMPTY_LIST;
	}
	
	
	@ApiOperation("Use this method to find all suffixes on the internal repository which match the ownerName, as the input parameter")
	@GetMapping(value = "/train/service/uri/findAll/suffix/byownername/{ownerName}/", produces = MediaType.APPLICATION_JSON_VALUE)
	List<Suffix> findAllSuffixByOwnerName(@ApiParam("The unique identifier, called here as ownername. The name of the owner of the PID")@PathVariable String ownerName) throws Exception {
		List<Suffix> allList = new ArrayList<Suffix>();
		allList = facade.findAllSuffixesByOwnerName(ownerName);
		
		if(!allList.isEmpty() || allList !=null) {
			return allList;
		}
		return Collections.EMPTY_LIST;
	}
	
	@ApiOperation("Use this method to find all suffixes on the internal repository which match the prefix, as the input parameter")
	@GetMapping(value = "/train/service/uri/findAll/suffix/byprefix/{prefix}/", produces = MediaType.APPLICATION_JSON_VALUE)
	List<Suffix> findAllSuffixByPrefix(@ApiParam("Prefix is the object which contains the number which represents the prefix of the URI, as well as it's related to one and only one owner and mutiple suffix")@PathVariable String prefix) throws Exception {
		List<Suffix> allList = new ArrayList<Suffix>();
		allList = facade.findAllSufixesByPrefix(prefix);
		
		if(!allList.isEmpty() || allList !=null) {
			return allList;
		}
		return Collections.EMPTY_LIST;
	}
	
	//--Sufix by properties
	

	@ApiOperation("Use this method to find all suffixes on the internal repository which match the prefix and version, as the input parameters")
	@GetMapping(value = "/train/service/uri/findAll/suffix/byprefixsuffixversion/{prefix}/{version}", produces = MediaType.APPLICATION_JSON_VALUE)
	List<String> findAllSuffixByPrefixAndVersion(
			@ApiParam("Prefix is the object which contains the number which represents the prefix of the URI, as well as it's related to one and only one owner and mutiple suffix")@PathVariable String prefix,
			@ApiParam("The version field aims to track the chagens on the PID along the time")@PathVariable String version) throws Exception {
		List<String> allList = new ArrayList<String>();
		allList = facade.findAllSufixesByPrefixVersion(prefix,version);
		
		if(!allList.isEmpty() || allList !=null) {
			return allList;
		}
		return Collections.EMPTY_LIST;
	}
	
	@ApiOperation("Use this method to find all suffixes on the internal repository which match the prefix and state, as the input parameters")
	@GetMapping(value = "/train/service/uri/findAll/suffix/byprefixstate/{prefix}/{state}", produces = MediaType.APPLICATION_JSON_VALUE)
	List<String> findAllSufixesByPrefixSuffixState(
			@ApiParam("Prefix is the object which contains the number which represents the prefix of the URI, as well as it's related to one and only one owner and mutiple suffix")@PathVariable String prefix, 
			@ApiParam("The state field aims to tag the PID object based on his lifecycle")@PathVariable SufixStateEnum state) throws Exception {
		List<String> allList = new ArrayList<String>();
		allList = facade.findAllSufixesByPrefixState(prefix,state);
		
		if(!allList.isEmpty() || allList !=null) {
			return allList;
		}
		return Collections.EMPTY_LIST;
	}
	
	@ApiOperation("Use this method to find all suffixes on the internal repository which match the prefix, version and state, as the input parameters")
	@GetMapping(value = "/train/service/uri/findAll/suffix/byprefixversionstate/{prefix}/{version}/{state}", produces = MediaType.APPLICATION_JSON_VALUE)
	List<String> findAllSufixesByPrefixVersionState(
			@ApiParam("Prefix is the object which contains the number which represents the prefix of the URI, as well as it's related to one and only one owner and mutiple suffix")@PathVariable String prefix, 
			@ApiParam("The version field aims to track the chagens on the PID along the time")@PathVariable String version, 
			@ApiParam("The state field aims to tag the PID object based on his lifecycle")@PathVariable SufixStateEnum state) throws Exception {
		List<String> allList = new ArrayList<String>();
		allList = facade.findAllSufixesByPrefixVersionState(prefix,version,state);
		
		if(!allList.isEmpty() || allList !=null) {
			return allList;
		}
		return Collections.EMPTY_LIST;
	}
	
	
	
	
	//================== URI ===============================
	
	@ApiOperation("Use this method to find all URIs on the internal repository which match the prefix and suffix, as the input parameters")
	@GetMapping(value = "/train/service/uri/findAll/byprefixsuffix/{prefix}/{suffix}", produces = MediaType.APPLICATION_JSON_VALUE)
	List<String> findAllURIByPrefixSuffix(
			@ApiParam("Prefix is the object which contains the number which represents the prefix of the URI, as well as it's related to one and only one owner and mutiple suffix")@PathVariable String prefix, 
			@ApiParam("The aims the suffix object is to provide, in combination with the preffix, a compete URI for the and user")@PathVariable String suffix) throws Exception {
		List<String> allList = new ArrayList<String>();
		allList = facade.findAllURIByPrefixSuffix(prefix,suffix);
		
		if(!allList.isEmpty() || allList !=null) {
			return allList;
		}
		return Collections.EMPTY_LIST;
	}

	
	@ApiOperation("Use this method to find all URIs on the internal repository which match the prefix, suffix and state as the input parameters")
	@GetMapping(value = "/train/service/uri/findAll/byprefixsuffixstate/{prefix}/{suffix}/{state}", produces = MediaType.APPLICATION_JSON_VALUE)
	List<String> findAllURIByPrefixSuffixState(
			@ApiParam("Prefix is the object which contains the number which represents the prefix of the URI, as well as it's related to one and only one owner and mutiple suffix")@PathVariable String prefix, 
			@ApiParam("The aims the suffix object is to provide, in combination with the preffix, a compete URI for the and user")@PathVariable String suffix, 
			@ApiParam("The state field aims to tag the PID object based on his lifecycle")@PathVariable SufixStateEnum state) throws Exception {
		List<String> allList = new ArrayList<String>();
		allList = facade.findAllURIByPrefixSuffixState(prefix, suffix,state);
		
		if(!allList.isEmpty() || allList !=null) {
			return allList;
		}
		return Collections.EMPTY_LIST;
	}
	
	@ApiOperation("Use this method to find all URIs on the internal repository which match the prefix, version and state as the input parameters")
	@GetMapping(value = "/train/service/uri/findAll/byprefixversionstate/{prefix}/{version}/{state}", produces = MediaType.APPLICATION_JSON_VALUE)
	List<String> findAllURIByPrefixVersionState(
			@ApiParam("Prefix is the object which contains the number which represents the prefix of the URI, as well as it's related to one and only one owner and mutiple suffix")@PathVariable String prefix, 
			@ApiParam("The version field aims to track the chagens on the PID along the time")@PathVariable String version, 
			@ApiParam("The state field aims to tag the PID object based on his lifecycle")@PathVariable SufixStateEnum state) throws Exception {
		List<String> allList = new ArrayList<String>();
		allList = facade.findAllURIByPrefixVersionState(prefix,version,state);
		
		if(!allList.isEmpty() || allList !=null) {
			return allList;
		}
		return Collections.EMPTY_LIST;
	}


	@GetMapping(value = "/train/service/uri/new/username/", produces = MediaType.APPLICATION_JSON_VALUE)
	String givenUsingJava8_whenGeneratingRandomAlphanumericString() throws Exception {
		return TrainUtil.givenUsingJava8_whenGeneratingRandomAlphanumericString();
	}
	
	
	//====== POST Methods to create the PID or t4fdoi object ============
	
	
	@ApiOperation("Use this method to request a new PID/DOI generation, where the ownername (as the input parameters) represents the person/company/organization related to that new PID Object")
	@PostMapping(value = "/train/service/uri/generate/pid/{ownername}",  produces = MediaType.APPLICATION_JSON_VALUE)
	Train4FAIRObject generatePID(
			@ApiParam("The unique identifier, called here as ownername. The name of the owner of the PID")@PathVariable String ownername) throws Exception {
		Train4FAIRObject pid = new Train4FAIRObject();

		pid = facade.generatePID(ownername);
		return pid;
	}
	
	@ApiOperation("Use this method to request a new suffix for an existing doi (which should hold an preffix), where the prefix, version and state are the mandatory input parameters")
	@PostMapping(value = "/train/service/uri/add/suffix/{prefix}/{version}/{state}" , produces = MediaType.APPLICATION_JSON_VALUE)
	Train4FAIRObject attachSuffixToPID(
			@ApiParam("Prefix is the object which contains the number which represents the prefix of the URI, as well as it's related to one and only one owner and mutiple suffix")@PathVariable String prefix, 
			@ApiParam("The version field aims to track the chagens on the PID along the time")@PathVariable String version, 
			@ApiParam("The state field aims to tag the PID object based on his lifecycle")@PathVariable SufixStateEnum state) throws Exception {

		Train4FAIRObject pid = new Train4FAIRObject();

		pid = facade.attachSuffixVersionAndStateToPID(prefix,version,state);
		return pid;
	}
	
	
	@ApiOperation("Use this method to edit the suffix parameters, such as version (the only two editable input parameters for this method). Plus the prefix and suffix are mandatory imputa parameters to edit the right object")
	@PostMapping(value = "/train/service/uri/update/suffix/{prefix}/{suffix}/{version}/{state}"
			+ "" , produces = MediaType.APPLICATION_JSON_VALUE)
	Train4FAIRObject editAttachedSuffixToPID(
			@ApiParam("Prefix is the object which contains the number which represents the prefix of the URI, as well as it's related to one and only one owner and mutiple suffix")@PathVariable String prefix, 
			@ApiParam("The version field aims to track the chagens on the PID along the time")@PathVariable String version, 
			@ApiParam("The state field aims to tag the PID object based on his lifecycle")@PathVariable SufixStateEnum state,
			@ApiParam("The aims the suffix object is to provide, in combination with the preffix, a compete URI for the and user")@PathVariable String suffix) throws Exception {

		Train4FAIRObject pid = new Train4FAIRObject();

		pid = facade.editAttachedSuffixToPID(prefix,suffix,version,state);
		return pid;
	}
	
	


}
