package de.fraunhofer.fit.train.model;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

@Api(value = "State aims to be a sort of flag to track the asset state.",
tags = {"SufixStateEnum"})
public enum SufixStateEnum {
	
	@ApiModelProperty(notes = "The DARFT state means that the asset under the requested suffix still under development.", 
            example = "The supported values are DRAFT, PUBLISHED or DEPRICATED.", position = 0)
	DRAFT("DRAFT"), 
	
	
	@ApiModelProperty(notes = "The PUBLISHED state means that the asset which was under development, now is ready.", 
    example = "The supported values are DRAFT, PUBLISHED or DEPRICATED.", position = 1)
	PUBLISHED("PUBLISHED"), 
	
	@ApiModelProperty(notes = "The PUBLISHED state means that the asset which was ready now is deprecated.", 
    example = "The supported values are DRAFT, PUBLISHED or DEPRICATED.", position = 2)
	DEPRICATED("DEPRICATED");
	
	
	private String text;

	SufixStateEnum(String text) {
		this.text = text;
	}

	public String getText() {
		return this.text;
	}
	

	public String setText(String text) {
		return this.text = text;
	}

	public static SufixStateEnum fromString(String text) {
		for (SufixStateEnum b : SufixStateEnum.values()) {
			if (b.text.equalsIgnoreCase(text)) {
				return b;
			}
		}
		return null;
	}


}
