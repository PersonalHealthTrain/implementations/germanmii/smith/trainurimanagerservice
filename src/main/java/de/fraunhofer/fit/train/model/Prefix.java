package de.fraunhofer.fit.train.model;

import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Repository;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

@Api(value = "Prefix object model aims to hold the list o suffic objects based on a singloe owner",
tags = {"prefix"})
@Repository
@Document(collection = "")
public class Prefix {
	
	@ApiModelProperty(notes = "Unique and Internal identifier of the related objet. This field is randomly generated by the platform.", 
            example = "Auto Generated",  position = 0)
	@Id
	@SerializedName("_id")
	@Expose
	private ObjectId _id;
	
	
	
	@ApiModelProperty(notes = "Unique identifier of the prefix object represented as a String. This field is randomly generated by the platform.", 
            example = "The PID Prefix as String. eg. 5e42cef39296cb2b74b2b15b", position = 1)
	@SerializedName("prefixStr")
	@Expose
	private String prefixStr;
	
	@ApiModelProperty(notes = "PrefixURI aims to hold the concatenation between the parameterized, PROTOCOL + HOST + PORT plus the generated prexis. "
			+ "This URI is just part of the work and just could be resolvable and finadable after the cliente of service start to attach suffixes. The the Suffix object will hold the completed URI. This field is randomly generated by the platform.", 
            example = "The prefix URI generated for a given suffix. eg. http://167.172.175.112:6666/5e42cef39296cb2b74b2b15b/",  position = 2)
	@SerializedName("prefixURI")
	@Expose
	private String prefixURI;
	
	
	@ApiModelProperty(notes = "The ownername aims to be the key to achieve the navigation on the object graph.", 
            example = "FIT, JBJ, MDK etc..", required = true, position = 3)
	@SerializedName("ownername")
	@Expose
	private String ownerName;
	
	
	
	@ApiModelProperty(notes = "The Owner Properties field aims to hold the PID properties in a generic and flexible way (as key/pair value). Given support for different requirements and use cases. Plus this field should be informed by the user of this service.", 
            example = "Author/JoaoChaves", required = true, position = 4)
	@SerializedName("ownerProperties")
	@Expose
	private Map<String,String> ownerProperties;


	public ObjectId get_id() {
		return _id;
	}


	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	
	public String getPrefixStr() {
		return prefixStr;
	}


	public void setPrefixStr(String prefixStr) {
		this.prefixStr = prefixStr;
	}


	public String getOwnerName() {
		return ownerName;
	}


	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}


	public Map<String, String> getOwnerProperties() {
		return ownerProperties;
	}


	public void setOwnerProperties(Map<String, String> ownerProperties) {
		this.ownerProperties = ownerProperties;
	}


	public String getPrefixURI() {
		return prefixURI;
	}


	public void setPrefixURI(String prefixURI) {
		this.prefixURI = prefixURI;
	}
	
	

}
