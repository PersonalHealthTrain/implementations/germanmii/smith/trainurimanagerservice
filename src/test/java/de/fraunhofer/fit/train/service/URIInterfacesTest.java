package de.fraunhofer.fit.train.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.HttpGet;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
@ComponentScan({ "de.fraunhofer.fit.train" })
@EntityScan("de.fraunhofer.fit.train")
@SpringBootApplication
public class URIInterfacesTest {
	
	
	//TODO: After ALL! Create the method to load the database with the preconfigured data
	//TODO: Don't forget to start testing the db services and comunication first 
	
	@Ignore
	@Test
	public void findAllObjectOnTheRepositorySuccess() throws IOException {

		//TODO: create some sample content and save under resources/test. Then Use it here to compare if the test is passing or not
		////String content = TrainUtil.readFileToStr(TRAINJS_FIRSTCALL_JSON_FILE_LOCATION);
		HttpGet httpGet = new HttpGet("http://127.0.0.1:8888/RepositoryService/train/service/uri/findAll/repositorydoiobj/");

		URL url = new URL("http://127.0.0.1:8888/RepositoryService/train/service/uri/findAll/repositorydoiobj/");
		URLConnection con = url.openConnection();
		InputStream in = con.getInputStream();
		String encoding = con.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;
		String body = IOUtils.toString(in, encoding);
		System.out.println(body);

		//TODO: Ensure the body is not null as well as if it contains the necessary information to make the test pass.
		Assert.assertNotNull(body);
	}
	
	@Ignore
	@Test
	public void findAllObjectOnTheRepositoryFail() throws Exception {
		//TODO: create some sample content and save under resources/test. Then Use it here to compare if the test is passing or not
		////String content = TrainUtil.readFileToStr(TRAINJS_FIRSTCALL_JSON_FILE_LOCATION);
		HttpGet httpGet = new HttpGet("http://127.0.0.1:8888/RepositoryService/train/service/uri/findAll/repositorydoiobj/");

		URL url = new URL("http://127.0.0.1:8888/RepositoryService/train/service/uri/findAll/repositorydoiobj/");
		URLConnection con = url.openConnection();
		InputStream in = con.getInputStream();
		String encoding = con.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;
		String body = IOUtils.toString(in, encoding);
		System.out.println(body);

		//TODO: Ensure the body is null OR if it contains OR not any necessary information to make the test to not pass.
		Assert.assertNull(body);
	}

	
	//TODO: Before ALL! Create the method to remove the database with the pre configured data
	
	
	//Simple Tests
	
	@Test
	public void givenUsingJava8_whenGeneratingRandomAlphanumericString_thenCorrect() {
	    int leftLimit = 48; // numeral '0'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = 3;
	    Random random = new Random();
	 
	    String generatedString = random.ints(leftLimit, rightLimit + 1)
	      .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();
	 
	    System.out.println(generatedString);
	}
	
	
}
